import glob
import time
from subprocess import Popen, PIPE


def main(data_dir, data_set, metric, n_results=1):

    # data_dir = './../datos'
    # data_set = 'TREC6'
    # metric = 'P.10'  # [p.10, p.100]

    inputs_list = glob.glob('{}/{}/todas_pooled/input.*'.format(data_dir, data_set))

    x = {}
    for elem in inputs_list:
        name = elem.split('input.')[1]
        p = Popen(['./../../trec_eval/trec_eval', '-m', metric, '{}/{}/qrels.official'.format(data_dir, data_set),
                   '{}/{}/todas_pooled/input.{}'.format(data_dir, data_set, name)],
                  stdin=PIPE, stdout=PIPE, stderr=PIPE)
        output = p.communicate()[0].strip()
        x[name] = output.decode("utf-8").split('\t')[2].split("\n")[0]

    ordered_results = [(k, v) for k, v in sorted(x.items(), key=lambda item: item[1], reverse=True)]
    print("Results for metric {} at dataset {}".format(metric, data_set))
    for result in ordered_results[0:n_results]:
        print(result)


if __name__ == '__main__':
    tic = time.time()
    data_dir = './../datos'
    for d_set in ['TREC5', 'TREC6', 'TREC7', 'TREC8']:
        for m in ['P.10', 'P.100']:
            main(data_dir, d_set, m)
    print("Execution time: {}".format(time.time() - tic))
