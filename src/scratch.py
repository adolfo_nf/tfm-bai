import pandas as pd
import numpy as np
import glob
import time
import sys
import os
from subprocess import Popen, PIPE


def precision_at(n, data_frame):
    """Return the n first elements of the dataframe

    :param n: number of items to select
    :param data_frame: dataframe to select data
    :return: dataframe with n rows
    """
    return data_frame.head(n)


def load_reference_data(data_dir=None):
    """Load reference data
    :param data_dir: data directory
    :return qrels: relevance judgements on files
    """
    # default data dir
    if data_dir is None:
        data_dir = './../datos'

    qrels = pd.read_csv(data_dir + '/qrels.official',
                        sep=' ',
                        names=['query_id', 'Q0', 'document_name', 'value'])

    return qrels


def calculate_low_bound_best_arm(mu, plays, K, N, delta):
    lbba = None
    return lbba


def eval_p_at_10(arm, query):
    return None


def racing_algorithm_hoeffding(active_arms, K, N, delta=0.05, R=1):
    """Runs the Hoeffding algorithm.

        Keyword arguments:
        active_arms -- list of active arms
        R           -- number of recommended arms (default 1)
        delta       -- significance level (default 0.05)
        N           -- Maximum number of rounds
    """
    cumsum = [0]*K
    mu = [0]*K
    plays = [0]*K


    querys = np.random.permutation(N)

    best_arm = None

    for query in querys:

        for arm in active_arms:

            # TODO eval p@10 for query to get reward
            reward = eval_p_at_10(arm, query)

            cumsum[arm] += reward
            plays[arm] += 1

            mu[arm] = cumsum[arm]/plays[arm]

        best_arm = arm

        if len(active_arms) == 1:
            return best_arm  # active_arm?


def main():
    tic = time.time()
    data_dir = './../datos'
    results_dir = './../results'
    n_top = 10
    # Query relevances
    qrels = load_reference_data()

    inputs_list = glob.glob(data_dir+'/todas_pooled/input.*')
    print(inputs_list)
    K = len(inputs_list)
    print("Number of arms (K): {}".format(K))



    data = {}
    system_dict = dict()
    i = 0
    if not os.path.exists(data_dir+'/todas_extended'):
        os.makedirs(data_dir+'/todas_extended')

    if not os.path.exists(results_dir):
        os.makedirs(results_dir)

    p10_list = []
    _system = []
    _p10 = []
    _p10_trec_eval = []

    advisory_effort = {}
    # inputs_list = ['./../datos/todas_pooled/input.anu5mrg0']
    # inputs_list = ['./../datos/todas_pooled/input.vtwnB1']
    #inputs_list = ['./../datos/todas_pooled/input.umcpa1']
    #inputs_list = ['./../datos/todas_pooled/input.gmu96ma1']
    #inputs_list = ['./../datos/todas_pooled/input.erliA1']
    for input_file in inputs_list[0:3]:
        print(os.path.basename(input_file))
        input_filename = os.path.basename(input_file)

        input_data = pd.read_csv(input_file, delim_whitespace=True, names=['query_id', 'Q0', 'document_name',
                                                                           'position', 'weight', 'system_name'])

        top10 = input_data.groupby('query_id').head(10)
        # for i in range(251, 301):
        #     print(top10[top10['query_id'] == i])

        # print("INDEX: {}".format(len(top10.index)))

        #result = pd.merge(top10[top10['query_id']==251], qrels, left_on=['query_id', 'document_name'], right_on=['query_id', 'document_name'])
        #print(result[['query_id', 'document_name']])
        new_relevance = []
        c = 0
        for index, row in top10.iterrows():
            c += 1
            sel_qrel = qrels[(qrels['query_id'] == row['query_id']) & (qrels['document_name'] == row['document_name'])]

            if sel_qrel.empty:
                print("# file {} not found in qrels for query {} at system {}".format(row['document_name'],
                                                                                      row['query_id'],
                                                                                      input_data['system_name'].iloc[0]))
                new_relevance.append(0)
            else:
                # print(len(sel_qrel))
                # print(sel_qrel)
                new_relevance.append(sel_qrel.iloc[0]['value'])

        # print("C = {}".format(c))
        #print(new_relevance)

        top10 = top10.assign(value=pd.Series(new_relevance).values)

        # for i in range(251, 301):
        #     print(top10[top10['query_id'] == i])

        # averages = list()
        # sorted_queries = sorted(set(input_data['query_id']))
        # document_name_dictlist = dict()
        # for query in sorted_queries:
        #
        #     selection = input_data[input_data['query_id'] == query].head(n_top)
        #
        #     document_name_dictlist[query] = list(selection['document_name'])
        #
        #     relevance = []
        #     for d_name in list(selection['document_name']):
        #         # print("D-name: {}".format(d_name))
        #         selected_qrel = qrels[(qrels['query_id'] == query) & (qrels['document_name'] == d_name)]
        #         if selected_qrel.empty:
        #             # print("file {} not found in qrels for query {} at system {}".format(d_name,
        #             #                                                                     query,
        #             #                                                                     input_data['system_name'].iloc[0]))
        #             relevance.append(0)
        #         else:
        #             relevance.append(selected_qrel.iloc[0]['value'])
        #     selection = selection.assign(relevance=pd.Series(relevance).values)
        #     averages.append(selection['relevance'].mean())
        #

        search_system_name = input_data.system_name.iloc[0]
        for k, v in top10.groupby('query_id')['document_name']:
            if k not in advisory_effort.keys():
                advisory_effort[k] = list(v)
            else:
                advisory_effort[k] += list(v)
            # print("VALUE {}\n{}".format(k, list(v)))

        print(advisory_effort[251])
        print(list(top10.groupby('query_id')['value'].sum()))
        sys.exit()
        # print(len(list(top10.groupby('query_id')['value'].sum())))
        # print(np.sum([x/10 for x in list(top10.groupby('query_id')['value'].sum())])/50)
        #print(top10[top10['query_id'] == 261])
        averages = [x/10 for x in list(top10.groupby('query_id')['value'].sum())]  #.mean())
        print("AVERAGES: {}".format(averages))
        #sys.exit()
        p_at_10 = np.mean(averages)
        # print(p_at_10)
        # sys.exit()
        system_dict[search_system_name] = {'P@10': p_at_10}

        # z = subprocess.call(['./../../trec_eval/trec_eval', '-m', 'P.10', './../datos/qrels.official',
        #                  './../datos/todas_pooled/input.{}'.format(search_system_name)])
        p = Popen(['./../../trec_eval/trec_eval', '-m', 'P.10', './../datos/qrels.official',
                   './../datos/todas_pooled/input.{}'.format(search_system_name)],
                  stdin=PIPE, stdout=PIPE, stderr=PIPE)

        output = p.communicate()[0]
        print("P@10: {:.4f} ::: {}".format(p_at_10, output.decode("utf-8").split('\t')[2].split("\n")[0]))
        p10_list.append([p_at_10, output.decode("utf-8").split('\t')[2].split("\n")[0]])

        data[input_data.system_name.iloc[0]] = averages

        _system.append(input_data.system_name.iloc[0])
        _p10.append(p_at_10)
        _p10_trec_eval.append(output.decode("utf-8").split('\t')[2].split("\n")[0])

        # if averages != list(top10.groupby('query_id')['value'].mean()):
        #     print("equal FALSE: \n{} ".format(averages != list(top10.groupby('query_id')['value'].mean())))

        # if i > 9:
        #     break
        # i += 1

    # if len(sorted_queries) > 0:
    #     data['query'] = sorted_queries
    df = pd.DataFrame(data, columns=list(data.keys()))
    print(df.head(10))

    for k, v in system_dict.items():
        print("{} ::: {}".format(k, v['P@10']))

    systems = {"system": _system, "p@10_trec_eval": _p10_trec_eval, "p@10": ["%.4f" % element for element in _p10]}
    df2 = pd.DataFrame(systems, columns=list(systems.keys()))
    print(df2.head(10))

    df2.to_csv(results_dir + '/systems.txt', index=False, sep=" ")

    print("\nadvisory_effort")
    ae_query = []
    ae_effort = []

    for k, v in advisory_effort.items():
        ae_query.append(k)
        ae_effort.append(len(set(v)))
        # print(k, len(set(v))/len(v))
    advisory_effort_df = pd.DataFrame({'query':ae_query, 'advisory_effort':ae_effort},
                                      columns=['query', 'advisory_effort'])
    advisory_effort_df.to_csv(results_dir+'/advisory_effort.csv', index=False)

    print("Execution time: {}".format(time.time() - tic))


if __name__ == "__main__":
    main()
