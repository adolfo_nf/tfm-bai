import pandas as pd
import glob
import time
import sys
import os


def precision_at(n, data_frame):
    """Return the n first elements of the dataframe

    :param n: number of items to select
    :param data_frame: dataframe to select data
    :return: dataframe with n rows
    """
    return data_frame.head(n)


def load_reference_data(data_dir=None):
    """Load reference data
    :param data_dir: data directory
    :return qrels: relevance judgements on files
    """
    # default data dir
    if data_dir is None:
        data_dir = './../datos'

    qrels = pd.read_csv(data_dir + '/qrels.official',
                        sep=' ',
                        names=['query_id', 'Q0', 'document_name', 'value'])

    return qrels


def main():
    tic = time.time()

    data_dir = './../datos'
    # Query relevances
    qrels = load_reference_data(data_dir)

    inputs_list = glob.glob(data_dir+'/todas_pooled/input.*')
    print(inputs_list)

    if not os.path.exists(data_dir+'/todas_10'):
        os.makedirs(data_dir+'/todas_10')

    for input_file in inputs_list:
        print(os.path.basename(input_file))
        input_filename = os.path.basename(input_file)
        input_data = pd.read_csv(input_file, delim_whitespace=True, names=['query_id', 'Q0', 'document_name',
                                                                           'position', 'weight', 'system_name'])
        relevance = []
        for index, row in input_data.iterrows():
            selected_qrel = qrels[(qrels['query_id'] == row['query_id']) &
                                  (qrels['document_name'] == row['document_name'])]
            if selected_qrel.empty:
                #print("file {} not found in qrels for query {} at system {}".format(row['document_name'],
                #                                                                    row['query_id'],
                #                                                                    input_data['system_name'].iloc[0]))
                # sys.exit()
                relevance.append(0)
            elif len(selected_qrel.index) > 1:
                print("Warning: several files for the same judgement")
                relevance.append(0)
            else:
                relevance.append(selected_qrel.iloc[0]['value'])

        input_data['relevance'] = relevance
        print(input_data)
        #export_data = input_data.to_csv(data_dir+'/todas_extended'+'/ext_'+input_filename)
        break
    print("fin")

if __name__ == '__main__':
    main()
