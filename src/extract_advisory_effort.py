import json
import sys


def load_effort_data(data_dir=None, data_set=None, metric='p_at_10'):
    """Load precision data
    :param data_dir: data directory
    :param data_set: data set (TREC5, ..., TREC8)
    :param metric: metric (p_at_10, p_at_100, ...)
    :return effort: effort for queries on files
    """
    # default data dir
    if data_dir is None:
        data_dir = './../datos'

    if data_set is None:
        data_set = 'TREC5'

    if metric == 'p_at_10':
        with open(data_dir + '/' + data_set + '/effort_10.csv') as f:
            effort = json.load(f)

    elif metric == 'p_at_100':
        with open(data_dir + '/' + data_set + '/effort_100.csv') as f:
            effort = json.load(f)
    else:
        print('Invalid metric.\nAlgorithm aborted."')
        sys.exit()
    return effort


def main():

    if len(sys.argv) == 1:
        # Data to load
        data_dir = './../datos'
        data_set = 'TREC5'
        metric = 'p_at_10'

    elif len(sys.argv) == 4:
        data_dir = sys.argv[1]
        data_set = sys.argv[2]
        metric = sys.argv[3]
    else:
        print("Wrong number of arguments.")
        print("Use: python extract_advisory_effort.py data_dir data_set metric")
        print("Example: python extract_advisory_effort.py ./../datos TREC5 p_at_10")
        sys.exit()

    # Load file
    effort = load_effort_data(data_dir, data_set, metric)

    # Calculate advisory effort
    ae_dict = dict()  # dictionary for advisory effort at each query
    for k1 in effort.keys():
        for k2 in effort[k1].keys():
            if k2 in ae_dict.keys():  # concatenate lists for same query
                ae_dict[k2] += effort[k1][k2]
            else:
                ae_dict[k2] = effort[k1][k2]

    advisory_effort = sum([len(set(ae_dict[k3])) for k3 in ae_dict.keys()])

    print("Advisory effort: {}".format(advisory_effort))


if __name__ == "__main__":
    main()
