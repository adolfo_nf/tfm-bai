import numpy as np
import time
import plotly
import plotly.graph_objs as go


def main():
    r = 1  # range of rewards (default 1)
    n = 50  # maximum number of plays
    delta = 0.05  # significance level (default 0.05)
    c = 4  # constant
    data = dict()
    data_successive = dict()
    for k in [1, 46, 71, 84, 101]:  # number of systems
        data_successive[k] = [np.sqrt(np.log(plays * plays * k * 4 / delta) / plays) for plays in range(1, n + 1)]

    x_values = list(range(1, n + 1))

    dt_scatters_successive = [go.Scatter(x=x_values, y=data_successive[x], mode='lines', name='{} - system'.format(x))
                              for x in data_successive.keys()]
    plotly.offline.plot({
        "data": dt_scatters_successive,
        "layout": go.Layout(title="Alpha evolution at {} significance level".format(delta),
                            xaxis=dict(title='Plays'),
                            yaxis=dict(title='Alpha'))
    }, auto_open=True, show_link=True, image='png', image_filename='successive_boundaries')
    # Create traces
    # fig = go.Figure()
    # x_values = list(range(1, n+1))
    # fig.add_trace(go.Scatter(x=x_values, y=data[0],
    #                          mode='lines',
    #                          name='lines'))
    # fig.add_trace(go.Scatter(x=x_values, y=data[1],
    #                          mode='lines+markers',
    #                          name='lines+markers'))
    # fig.add_trace(go.Scatter(x=x_values, y=data[2],
    #                          mode='markers', name='markers'))
    #
    # fig.show()


if __name__ == "__main__":

    tic = time.time()
    main()
    print("Execution time: {}".format(time.time() - tic))
