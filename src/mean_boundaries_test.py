import numpy as np
import pandas as pd

def main(delta, eps):
    delta_original = delta
    epsilon_original = eps

    sep = ' '
    # print("\n")
    # print("-" * 50)
    # print("Delta: {}\nEpsilon: {}".format(delta, eps))
    delta = delta/2
    eps = eps/4

    print("-"*75)
    for n in range(1):
        times = 1/np.power(eps/2, 2)*np.log(3/delta)
        times10 = 1 / np.power(eps / 2, 2) * np.log10(3 / delta)
        print("{:3d}{:4s}{:.8f}{:4s}{:.8f}{:4s}{:.8f}{:4s}{:.8f}{:4s}{:.8f}{:4s}{:.8f}".format(n,
                                                                                    sep,
                                                                                    delta_original,
                                                                                    sep,
                                                                                    epsilon_original,
                                                                                    sep,
                                                                                    delta,
                                                                                    sep,
                                                                                    eps,
                                                                                    sep,
                                                                                    times,
                                                                                               sep,
                                                                                               times10))
        eps = 3/4*eps
        delta = delta/2

    return delta_original, epsilon_original, times, times10


if __name__ == "__main__":

    sep = ' '
    print("{:3s}{:4s}{:10s}{:4s}{:10s}{:4s}{:10s}{:4s}{:10s}{:4s}{:10s}".format(' n ', sep,
                                                                                'delta_o',
                                                                                sep,
                                                                                'eps_o',
                                                                                sep,
                                                                                'delta',
                                                                                sep,
                                                                                'epsilon',
                                                                                sep,
                                                                                'value'))

    results_list = list()
    for d in [0.05, 0.1, 0.2, 0.3]:  # loop in delta
        for e in np.linspace(0.1, 1.0, 10):  # loop in epsilon
            delta_original, epsilon_original, times, times10 = main(d, e)

            results_list.append([delta_original, epsilon_original, times, times10])
    df = pd.DataFrame(results_list, columns=['delta', 'epsilon', 'log', 'log10'])
    print(df.to_string(index=False))
    df.to_csv(r'./../results/mean_boundaries.csv', index=False, encoding='utf-8', sep=',')
