import pandas as pd
import numpy as np
import glob
import time
import sys
import pickle
import json


def load_precision_data(data_dir=None, data_set=None, metric='p_at_10'):
    """Load precision data
    :param data_dir: data directory
    :param data_set: data set (TREC5, ..., TREC8)
    :param metric: metric (p_at_10, p_at_100, ...)
    :return precision: relevance judgements on files
    """
    # default data dir
    if data_dir is None:
        data_dir = './../datos'

    if data_set is None:
        data_set = 'TREC5'

    if metric == 'p_at_10':
        precision = pd.read_csv(data_dir + '/' + data_set + '/count_10.csv', sep=' ', index_col=0
                                )
    elif metric == 'p_at_100':
        precision = pd.read_csv(data_dir + '/' + data_set + '/count_100.csv',
                                sep=' ',
                                index_col=0)
    else:
        print('Invalid metric.\nAlgorithm aborted."')
        sys.exit()
    return precision


def load_effort_data(data_dir=None, data_set=None, metric='p_at_10'):
    """Load precision data
    :param data_dir: data directory
    :param data_set: data set (TREC5, ..., TREC8)
    :param metric: metric (p_at_10, p_at_100, ...)
    :return effort: effort for queries on files
    """
    # default data dir
    if data_dir is None:
        data_dir = './../datos'

    if data_set is None:
        data_set = 'TREC5'

    if metric == 'p_at_10':
        with open(data_dir + '/' + data_set + '/effort_10.csv') as f:
            effort = json.load(f)

    elif metric == 'p_at_100':
        with open(data_dir + '/' + data_set + '/effort_100.csv') as f:
            effort = json.load(f)
    else:
        print('Invalid metric.\nAlgorithm aborted."')
        sys.exit()
    return effort


def create_arms_list(arms_names, k_arms):
    """Generates a list of Arms based on its names.
    :param arms_names: list of arms names
    :param k_arms: number of consecutive arms to select (None to select all)
    :return arms: a list of bandit arms
    """

    if k_arms is None:
        selected_arms = arms_names
    elif len(arms_names) < k_arms:
        print("Too much arms requested.\nAlgorithm aborted.")
        sys.exit()
    else:
        selected_arms = arms_names[0:k_arms]

    arms = [Arm(i, name) for i, name in enumerate(selected_arms)]

    return arms


def save_active_arms_to_file(active_arms, K, alg_name, metric, reward_order, delta, file_format='json'):
    # save active arms
    if file_format == 'binary':
        with open('./../results/final_arms_{}_{}_{}_{}_{}'.format(K, alg_name, metric, reward_order, delta), 'wb') as \
                final_arms_binary_file:
            pickle.dump(active_arms, final_arms_binary_file)
    elif file_format == 'json':
        with open('./../results/final_arms_{}_{}_{}_{}_{}.json'.format(K, alg_name, metric, reward_order, delta), 'w') as \
                final_arms_json_file:
            # Save a list of serialized Arms to json
            json.dump([a.__dict__ for a in active_arms], final_arms_json_file)
    else:
        print("Invalid file format {}.\nResults will not be written to file.".format(file_format))


def main():
    data_dir = './../datos'
    data_set = 'TREC5'  # Choose [TREC5, TREC6, TREC7, TREC8]
    metric = 'p_at_10'  # choose ['p_at_10', 'p_at_100']
    query_order = 'direct'  # Choose ['direct', 'random', 'reverse']
    """choose an algorithm:
    'hoeffding', or 'bernstein' for racing
    'successive' for elimination
    'lucb' for LUCB
    """
    algorithm_name = 'lucb'
    delta = 0.05  # significance level (default 0.05)

    k_arms = None
    write_results = True

    n_runs_max = 1

    '''Use from reference ones according to deltas (0.05, 0.1, 0.2, 0.3):
    p@10: [16033, 9676, 9815, 10678]
    p@100: [134618, 72258, 80554, 89297]
    '''
    best_advisory_effort = 16033

    # Load pre-computed data files
    precision = load_precision_data(data_dir, data_set, metric)
    effort = load_effort_data(data_dir, data_set, metric)

    arms_names = list(precision.index.values)

    n_runs = 0
    advisory_efforts = []
    while True:
        # Create the list of arms
        active_arms = create_arms_list(arms_names, k_arms)
        # Run algorithm
        if algorithm_name in ['hoeffding', 'bernstein']:
            bai, aa, removed_arms, advisory_effort = racing_algorithm(algorithm_name, active_arms, precision, effort,
                                                                      metric, query_order, delta, write_results)
        elif algorithm_name in ['successive']:
            bai, aa, removed_arms, advisory_effort = successive_elimination_algorithm(algorithm_name, active_arms,
                                                                                      precision, effort, metric,
                                                                                      query_order, delta, write_results)
        elif algorithm_name in ['lucb']:
            bai, aa, removed_arms, advisory_effort = lucb_algorithm(algorithm_name, active_arms,
                                                                    precision, effort, metric,
                                                                    query_order, delta, write_results)
        else:
            print("Invalid algorithm: {}.\nExecution aborted.".format(algorithm_name))
            sys.exit()

        n_runs += 1
        advisory_efforts.append(advisory_effort)
        if len(aa) != len(arms_names) and advisory_effort < best_advisory_effort:
            print("\nElimination found after {} runs for {} metric with {} query order.".format(n_runs,
                                                                                                metric,
                                                                                                query_order))
            bai.to_string()
            print("Removed arms: {}".format([x.name for x in removed_arms]))
            break
        elif n_runs >= n_runs_max:
            print("\nNo elimination after {} runs for {} metric with {} query order.".format(n_runs_max,
                                                                                             metric,
                                                                                             query_order))
            print("Advisory efforts average: {}".format(np.mean(advisory_efforts)))
            print("Advisory efforts median: {}".format(np.median(advisory_efforts)))
            print("Advisory efforts: {}".format(advisory_efforts))
            break
        else:
            pass


def racing_algorithm(alg_name, active_arms, precision, effort, metric, reward_order, delta=0.05,
                     save_to_file=False):
    """
    Run racing algorithms.

    :param alg_name: algorithm name ('hoeffding', 'bernstein')
    :param active_arms: list of active arms
    :param metric: metric to measure an arm
    :param precision: dataframe with counts for precision at given metric
    :param effort: dictionary with the effort files
    :param reward_order: order to evaluate queries ('direct', 'reverse', 'random')
    :param delta: significance level (default 0.05)
    :param save_to_file: boolean for save results to file
    :return best_arm: the best arm found
    """

    print("Running {}'s race".format(alg_name))
    # select metric (p@n)
    if metric == 'p_at_10':
        n_top = 10
    elif metric == 'p_at_100':
        n_top = 100
    else:
        print("Invalid metric {}.\nAlgorithm aborted.".format(metric))
        sys.exit()

    query_names = list(precision)

    if reward_order == 'direct':
        queries = query_names
    elif reward_order == 'reverse':
        queries = list(reversed(query_names))
    elif reward_order == 'random':
        # permute queries to get a random reward
        queries = np.random.permutation(query_names)
    else:
        print("Invalid order for queries {}.\nAlgorithm aborted.".format(reward_order))
        sys.exit()

    R = 1  # range of rewards (default 1)
    N = len(query_names)  # maximum number of rounds
    K = len(active_arms)  # number of systems
    # delta = 0.05  # significance level (default 0.05)

    # constant values for boundary
    logarithm_hoeffding = np.log(2 * K * N / delta)
    logarithm_berstein = np.log(3 * K * N / delta)

    print("N {} :: K {} :: delta {} :: log hoeffding {} :: log bernstein {}".format(N,
                                                                                    K,
                                                                                    delta,
                                                                                    logarithm_hoeffding,
                                                                                    logarithm_berstein))
    advisory_effort = 0
    advisory_effort_at_queries = dict()
    removed_arms = []
    for query in queries:
        print("\nQUERY #{}\n***************".format(query))

        effort_for_query = []
        for arm in active_arms:

            arm.plays += 1  # update plays
            arm.cumsum += precision.at[arm.name, query]/n_top  # accumulate
            # print(precision.at[arm.name, query])

            arm.rewards.append(precision.at[arm.name, query]/n_top)
            # arm.rewards.append(relevances_per_query/n_top)
            # print("arm.cumsum {} :: sum_arm_rewards {}".format(arm.cumsum, sum(arm.rewards)))
            arm.mu = arm.cumsum/arm.plays  # calculate new mean
            # print("arm.mu {} :: arm.mean {}".format(arm.mu, np.mean(arm.rewards)))

            if alg_name == 'hoeffding':
                arm.boundary = R * np.sqrt(logarithm_hoeffding/(2*arm.plays))
            elif alg_name == 'bernstein':
                arm.boundary = np.std(arm.rewards) * np.sqrt(2*logarithm_berstein/arm.plays) + \
                               (3*R*logarithm_berstein/arm.plays)
            else:
                print("Invalid algorithm: {}\nAlgorithm aborted.".format(alg_name))

            # print("{} :: {}".format(arm.name, query))
            effort_for_query += effort[arm.name][query]

        advisory_effort_at_queries[query] = len(set(effort_for_query))
        advisory_effort += len(set(effort_for_query))

        # Select best arm
        mus = [x.mu for x in active_arms]

        # print("Medias: {}".format(mus))
        # print(mus.index(max(mus)))

        best_arm = active_arms[mus.index(max(mus))]
        print("Best arm up to now:")
        best_arm.to_string()

        low_bound_best_arm = best_arm.mu - best_arm.boundary
        # print("low bound: {}".format(low_bound_best_arm))
        aa = []

        for a in active_arms:
            if a != best_arm and a.mu+a.boundary < low_bound_best_arm:
                print("{} will be drop".format(a.name))
                removed_arms.append(a)
                pass
                # active_arms.pop(a)
            else:
                aa.append(a)
        active_arms = aa
        print(".... there are still {} active arms".format(len(active_arms)))

        if len(active_arms) == 1:
            print("EUREKA! An unique winning arm has been found")
            print("Advisory effort: {}".format(advisory_effort))
            sys.exit()
            save_active_arms_to_file(active_arms, K, alg_name, metric, reward_order, delta)
            return active_arms[0]

    print("\nSorry! No satisfactory BAI output found")
    print("Advisory effort: {}".format(advisory_effort))
    print("\nBest arm found:")
    best_arm.to_string()
    print("Random query list: {}".format(queries))

    if save_to_file:
        save_active_arms_to_file(active_arms, K, alg_name, metric, reward_order, delta)

    return best_arm, active_arms, removed_arms, advisory_effort


def successive_elimination_algorithm(alg_name, active_arms, precision, effort, metric, reward_order,
                                     delta=0.05, save_to_file=False):
    """
    Run successive elimination algorithm.

    :param alg_name: algorithm name ('successive')
    :param active_arms: list of active arms
    :param metric: metric to measure an arm
    :param precision: dataframe with counts for precision at given metric
    :param effort: dictionary with the effort files
    :param reward_order: order to evaluate queries ('direct', 'reverse', 'random')
    :param delta: significance level (default 0.05)
    :param save_to_file: boolean for save results to file
    :return best_arm: the best arm found
    """

    print("Running {}".format(alg_name))
    # select metric (p@n)
    if metric == 'p_at_10':
        n_top = 10
    elif metric == 'p_at_100':
        n_top = 100
    else:
        print("Invalid metric {}.\nAlgorithm aborted.".format(metric))
        sys.exit()

    query_names = list(precision)

    if reward_order == 'direct':
        queries = query_names
    elif reward_order == 'reverse':
        queries = list(reversed(query_names))
    elif reward_order == 'random':
        # permute queries to get a random reward
        queries = np.random.permutation(query_names)
    else:
        print("Invalid order for queries {}.\nAlgorithm aborted.".format(reward_order))
        sys.exit()

    c = 4  # constant
    N = len(query_names)  # maximum number of rounds
    K = len(active_arms)  # number of systems

    advisory_effort = 0
    advisory_effort_at_queries = dict()
    removed_arms = []
    plays = 0
    for query in queries:
        plays += 1
        print("\nQUERY #{}\n***************".format(query))

        effort_for_query = []
        for arm in active_arms:

            arm.plays += 1  # update plays
            arm.cumsum += precision.at[arm.name, query]/n_top  # accumulate
            # print(precision.at[arm.name, query])

            arm.rewards.append(precision.at[arm.name, query]/n_top)
            # arm.rewards.append(relevances_per_query/n_top)
            # print("arm.cumsum {} :: sum_arm_rewards {}".format(arm.cumsum, sum(arm.rewards)))
            arm.mu = arm.cumsum/arm.plays  # calculate new mean
            # print("arm.mu {} :: arm.mean {}".format(arm.mu, np.mean(arm.rewards)))

            if alg_name == 'successive':
                alpha = np.sqrt(np.log(c * K * plays * plays / delta) / plays)
                arm.boundary = alpha
            else:
                print("Invalid algorithm: {}\nAlgorithm aborted.".format(alg_name))

            # print("{} :: {}".format(arm.name, query))
            effort_for_query += effort[arm.name][query]

        advisory_effort_at_queries[query] = len(set(effort_for_query))
        advisory_effort += len(set(effort_for_query))

        # Select best arm
        mus = [x.mu for x in active_arms]

        # print("Medias: {}".format(mus))
        # print(mus.index(max(mus)))

        best_arm = active_arms[mus.index(max(mus))]
        print("Best arm up to now:")
        best_arm.to_string()

        low_bound_best_arm = best_arm.mu - best_arm.boundary
        # print("low bound: {}".format(low_bound_best_arm))
        aa = []

        for a in active_arms:
            if a != best_arm and (best_arm.mu - a.mu) >= (2 * alpha):
                print("{} will be drop".format(a.name))
                removed_arms.append(a)
                pass
                # active_arms.pop(a)
            else:
                aa.append(a)
        active_arms = aa
        print(".... there are still {} active arms".format(len(active_arms)))

        if len(active_arms) == 1:
            print("EUREKA! An unique winning arm has been found")
            print("Advisory effort: {}".format(advisory_effort))
            sys.exit()
            save_active_arms_to_file(active_arms, K, alg_name, metric, reward_order, delta)
            return active_arms[0]

    print("\nSorry! No satisfactory BAI output found")
    print("Advisory effort: {}".format(advisory_effort))
    print("\nBest arm found:")
    best_arm.to_string()
    print("Random query list: {}".format(queries))

    if save_to_file:
        save_active_arms_to_file(active_arms, K, alg_name, metric, reward_order, delta)

    return best_arm, active_arms, removed_arms, advisory_effort


def lucb_algorithm(alg_name, active_arms, precision, effort, metric, reward_order,
                                     delta=0.05, save_to_file=False):
    """
    Run LUCB algorithm.

    :param alg_name: algorithm name ('lucb')
    :param active_arms: list of active arms
    :param metric: metric to measure an arm
    :param precision: dataframe with counts for precision at given metric
    :param effort: dictionary with the effort files
    :param reward_order: order to evaluate queries ('direct', 'reverse', 'random')
    :param delta: significance level (default 0.05)
    :param save_to_file: boolean for save results to file
    :return best_arm: the best arm found
    """

    print("Running {} for metric {} with {} order and delta={}".format(alg_name, metric, reward_order, delta))
    print("-"*80)

    print("{:5s}\t{:10s}\t{:10s}\t{:10s}".format("QUERY", "LOWER BOUND", "UPPER BOUND", "DIFFERENCE"))
    # select metric (p@n)
    if metric == 'p_at_10':
        n_top = 10
    elif metric == 'p_at_100':
        n_top = 100
    else:
        print("Invalid metric {}.\nAlgorithm aborted.".format(metric))
        sys.exit()

    query_names = list(precision)

    if reward_order == 'direct':
        queries = query_names
    elif reward_order == 'reverse':
        queries = list(reversed(query_names))
    elif reward_order == 'random':
        # permute queries to get a random reward
        queries = np.random.permutation(query_names)
    else:
        print("Invalid order for queries {}.\nAlgorithm aborted.".format(reward_order))
        sys.exit()

    N = len(query_names)  # maximum number of rounds
    K = len(active_arms)  # number of systems
    epsilon = 0.1

    advisory_effort = 0
    advisory_effort_at_queries = dict()

    bai_ids = []
    # Draw a reward for each arm
    query = queries[0]  # first query
    for arm in active_arms:
        arm.plays += 1  # update arm plays
        arm.cumsum += precision.at[arm.name, query] / n_top  # accumulate
        arm.rewards.append(precision.at[arm.name, query] / n_top)
        arm.mu = arm.cumsum / arm.plays  # calculate new mean

    removed_arms = []
    rounds = 1  # general plays
    for query in queries[1:]:  # all the remaining queries

        #print("\nQUERY #{}\n***************".format(query))

        # calculate boundaries for all active arms
        boundary_logarithm = np.log(5 * K * np.power(rounds, 4) / (4 * delta))  # constant on each round
        for arm in active_arms:
            arm.boundary = np.sqrt(boundary_logarithm / (2 * arm.plays))

        # Select best arm according to its mean
        mus = [x.mu for x in active_arms]
        index_best_arm = mus.index(max(mus))
        best_arm = active_arms[index_best_arm]

        bai_ids.append((best_arm.name, best_arm.mu))
        # Low Bound Best Arm
        low_bound_best_arm = best_arm.mu - best_arm.boundary

        # print("Best arm up to now:")
        # best_arm.to_string()

        index_hucb_arm = None
        hucb_arm = None
        arg_max = None
        for index, arm in enumerate(active_arms):
            if arm != best_arm:
                max_value = arm.mu + arm.boundary
                if arg_max is None or max_value > arg_max:
                    arg_max = max_value
                    index_hucb_arm = index
                    hucb_arm = arm
                else:
                    pass
            else:
                pass
                # print("Best arm not considered!!!")

        # other_arms = active_arms[:index_best_arm] + active_arms[index_best_arm + 1:]
        # other_arms_upper_boundaries = [x.mu + x.boundary for x in other_arms]
        # index_hucb_arm = other_arms_upper_boundaries.index(max(other_arms_upper_boundaries))
        # hucb_arm = other_arms[index_hucb_arm]

        # High Bound HUCB Arm
        high_bound_hucb_arm = hucb_arm.mu + hucb_arm.boundary
        # print("QUERY: {:3s} ::: {:.5f} ::: {:.5f} ::: diff: {:.5f}".format(query,
        #                                                   high_bound_hucb_arm, low_bound_best_arm,
        #                                                   high_bound_hucb_arm-low_bound_best_arm))

        print("{:>5s}\t{:10.5f}\t{:10.5f}\t{:10.5f}".format(query,
                                                                           high_bound_hucb_arm, low_bound_best_arm,
                                                                           high_bound_hucb_arm - low_bound_best_arm))
        if high_bound_hucb_arm - low_bound_best_arm < epsilon:
            print("BEST ARM FOUND!!!")
            return best_arm

        # print("Método 1: {}\nMétodo 2: {}\nINDEX BEST: {}\nINDEX: {}".format(n_hucb_arm.name, hucb_arm.name,
        #                                                                      index_best_arm, n_index_hucb_arm))
        #
        # best_arm.to_string()
        # hucb_arm.to_string()
        # sys.exit()
        # draw rewards and update
        best_arm.plays += 1  # update best_arm plays
        best_arm.cumsum += precision.at[best_arm.name, query] / n_top  # accumulate
        best_arm.rewards.append(precision.at[best_arm.name, query] / n_top)
        best_arm.mu = best_arm.cumsum / best_arm.plays  # calculate new mean

        hucb_arm.plays += 1  # update hucb_arm plays
        hucb_arm.cumsum += precision.at[hucb_arm.name, query] / n_top  # accumulate
        hucb_arm.rewards.append(precision.at[hucb_arm.name, query] / n_top)
        hucb_arm.mu = hucb_arm.cumsum / hucb_arm.plays  # calculate new mean

        rounds += 1

    #     effort_for_query = []
    #     for arm in active_arms:
    #
    #         arm.plays += 1  # update plays
    #         arm.cumsum += precision.at[arm.name, query]/n_top  # accumulate
    #         # print(precision.at[arm.name, query])
    #
    #         arm.rewards.append(precision.at[arm.name, query]/n_top)
    #         # arm.rewards.append(relevances_per_query/n_top)
    #         # print("arm.cumsum {} :: sum_arm_rewards {}".format(arm.cumsum, sum(arm.rewards)))
    #         arm.mu = arm.cumsum/arm.plays  # calculate new mean
    #         # print("arm.mu {} :: arm.mean {}".format(arm.mu, np.mean(arm.rewards)))
    #
    #         if alg_name == 'successive':
    #             alpha = np.sqrt(np.log(c * K * plays * plays / delta) / plays)
    #             arm.boundary = alpha
    #         else:
    #             print("Invalid algorithm: {}\nAlgorithm aborted.".format(alg_name))
    #
    #         # print("{} :: {}".format(arm.name, query))
    #         effort_for_query += effort[arm.name][query]
    #
    #     rounds += 1
    #
    #     advisory_effort_at_queries[query] = len(set(effort_for_query))
    #     advisory_effort += len(set(effort_for_query))
    #
    #
    #
    #     low_bound_best_arm = best_arm.mu - best_arm.boundary
    #     # print("low bound: {}".format(low_bound_best_arm))
    #     aa = []
    #
    #     for a in active_arms:
    #         if a != best_arm and (best_arm.mu - a.mu) >= (2 * alpha):
    #             print("{} will be drop".format(a.name))
    #             removed_arms.append(a)
    #             pass
    #             # active_arms.pop(a)
    #         else:
    #             aa.append(a)
    #     active_arms = aa
    #     print(".... there are still {} active arms".format(len(active_arms)))
    #
    #     if len(active_arms) == 1:
    #         print("EUREKA! An unique winning arm has been found")
    #         print("Advisory effort: {}".format(advisory_effort))
    #         sys.exit()
    #         save_active_arms_to_file(active_arms, K, alg_name, metric, reward_order, delta)
    #         return active_arms[0]
    #
    print("\nSorry! No satisfactory BAI output found")
    # print("Advisory effort: {}".format(advisory_effort))
    print("\nBest arm found:")
    best_arm.to_string()
    # print("Random query list: {}".format(queries))

    # add final best arm properties
    bai_ids.append((best_arm.name, best_arm.mu))
    print("\nBest arm series (name, mean):")
    print(*bai_ids, sep='\n')
    # for x in bai_ids:
    #     print(x)

    sys.exit()
    if save_to_file:
        save_active_arms_to_file(active_arms, K, alg_name, metric, reward_order, delta)

    return best_arm, active_arms, removed_arms, advisory_effort


class Arm(object):
    """
    Bandit Arm Class
    """
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.mu = 0.0
        self.cumsum = 0.0
        self.plays = 0
        self.boundary = None
        self.rewards = []

    def to_string(self):
        """
        Prints Arm as string
        """
        print("id: {} :: name: {} :: mu: {} :: cumsum: {} :: plays: {} :: boundary: {}".format(self.id,
                                                                                               self.name,
                                                                                               self.mu,
                                                                                               self.cumsum,
                                                                                               self.plays,
                                                                                               self.boundary))


if __name__ == "__main__":

    tic = time.time()
    main()
    print("Execution time: {}".format(time.time() - tic))
