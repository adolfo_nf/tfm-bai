import pandas as pd
import numpy as np
import glob
import time
import sys
import pickle
import json


def precision_at(n, data_frame):
    """Return the n first elements of the dataframe

    :param n: number of items to select
    :param data_frame: dataframe to select data
    :return: dataframe with n rows
    """
    return data_frame.head(n)


def load_reference_data(data_dir=None, data_set=None):
    """Load reference data
    :param data_dir: data directory
    :param data_set: data set (TREC5, ..., TREC8)
    :return qrels: relevance judgements on files
    """
    # default data dir
    if data_dir is None:
        data_dir = './../datos'

    if data_set is None:
        data_set = 'TREC5'
    qrels = pd.read_csv(data_dir + '/' + data_set + '/qrels.official',
                        sep=' ',
                        names=['query_id', 'Q0', 'document_name', 'value'])
    return qrels


def create_arms_list(k_arms):
    """
    :param k_arms: number of consecutive arms to select (None to select all)
    :return arms: a list of bandit arms
    """
    data_dir = './../datos'
    data_set = 'TREC5'
    arms_paths = glob.glob(data_dir + '/' + data_set+'/todas_pooled/input.*')

    if k_arms is None:
        selected_arms_paths = arms_paths
    elif len(arms_paths) < k_arms:
        print("Too much arms requested.\nAlgorithm aborted.")
        sys.exit()
    else:
        selected_arms_paths = arms_paths[0:k_arms]

    arms = [Arm(i, path) for i, path in enumerate(selected_arms_paths)]
    return arms


def save_active_arms_to_file(active_arms, K, alg_name, metric, reward_order, file_format='json'):
    # save active arms
    if file_format == 'binary':
        with open('./../results/final_arms_{}_{}_{}_{}'.format(K, alg_name, metric, reward_order), 'wb') as \
                final_arms_binary_file:
            pickle.dump(active_arms, final_arms_binary_file)
    elif file_format == 'json':
        with open('./../results/final_arms_{}_{}_{}_{}.json'.format(K, alg_name, metric, reward_order), 'w') as \
                final_arms_json_file:
            # Save a list of serialized Arms to json
            json.dump([a.__dict__ for a in active_arms], final_arms_json_file)
    else:
        print("Invalid file format {}.\nResults will not be written to file.".format(file_format))


def main():
    # select k arms
    k_arms = 3
    active_arms = create_arms_list(k_arms)
    # Run algorithm
    bai = racing_algorithm('hoeffding', active_arms, 'p_at_10', 'direct', True)


def racing_algorithm(alg_name, active_arms, metric, reward_order, save_to_file=False):
    """
    Run racing algorithms.

    :param alg_name: algorithm name ('hoeffding', 'bernstein')
    :param active_arms: list of active arms
    :param metric: metric to measure an arm
    :param reward_order: order to evaluate queries ('direct', 'reverse', 'random')
    :param save_to_file: boolean for save results to file
    :return best_arm: the best arm found
    """

    print("Running {}'s race".format(alg_name))
    # select metric (p@n)
    if metric == 'p_at_10':
        n_top = 10
    elif metric == 'p_at_100':
        n_top = 100
    else:
        metric == 'p_at_1'
        n_top = 1

    query_names = list(range(251, 301))
    if reward_order == 'direct':
        queries = query_names
    elif reward_order == 'reverse':
        queries = reversed(query_names)
    elif reward_order == 'random':
        # permute queries to get a random reward
        queries = np.random.permutation(query_names)
    else:
        print("Invalid order for queries {}.\nAlgorithm aborted.".format(reward_order))

    # load query relevances
    qrels = load_reference_data()

    R = 1  # range of rewards (default 1)
    N = len(query_names)  # maximum number of rounds
    K = len(active_arms)  # number of systems
    delta = 0.05  # significance level (default 0.05)

    # constant values for boundary
    logarithm_hoeffding = np.log(2 * K * N / delta)
    logarithm_berstein = np.log10(3 * K * N / delta)

    print("N {} :: K {} :: delta {} :: log hoeffding {} :: log bernstein {}".format(N,
                                                                                    K,
                                                                                    delta,
                                                                                    logarithm_hoeffding,
                                                                                    logarithm_berstein))

    for query in queries:
        print("\nQUERY #{}".format(query))

        for arm in active_arms:

            arm_data = pd.read_csv(arm.path, delim_whitespace=True, names=['query_id',
                                                                           'Q0',
                                                                           'document_name',
                                                                           'position',
                                                                           'weight',
                                                                           'system_name'])

            # calculate reward

            top_data = arm_data[arm_data['query_id'] == int(query)].head(n_top)

            relevances = []

            for index, row in top_data.iterrows():
                sel_qrel = qrels[(qrels['query_id'] == row['query_id']) &
                                 (qrels['document_name'] == row['document_name'])]

                if sel_qrel.empty:
                    # print("# file {} not found in qrels for query {} at system {}".format(row['document_name'],
                    #                                                                       row['query_id'],
                    #                                                                       arm_data['system_name'].iloc[0]))
                    relevances.append(0)
                else:
                    relevances.append(sel_qrel.iloc[0]['value'])

            top_data = top_data.assign(value=pd.Series(relevances).values)

            relevances_per_query = top_data.groupby('query_id')['value'].sum().values[0]

            arm.plays += 1  # update plays
            arm.cumsum += relevances_per_query/n_top  # accumulate
            arm.rewards.append(relevances_per_query/n_top)
            # print("arm.cumsum {} :: sum_arm_rewards {}".format(arm.cumsum, sum(arm.rewards)))
            arm.mu = arm.cumsum/arm.plays  # calculate new mean
            # print("arm.mu {} :: arm.mean {}".format(arm.mu, np.mean(arm.rewards)))

            if alg_name == 'hoeffding':
                arm.boundary = R * np.sqrt(logarithm_hoeffding/(2*arm.plays))
            elif alg_name == 'bernstein':
                arm.boundary = np.std(arm.rewards) * np.sqrt(2*logarithm_berstein/arm.plays) + \
                               (3*R*logarithm_berstein/arm.plays)
            else:
                print("Invalid algorithm: {}\nAlgorithm aborted.".format(alg_name))

            arm.advisory_effort_q[query] = list(top_data['document_name'])

            ae_effort = []  # advisory effort list
            for k, v in arm.advisory_effort_q.items():
                ae_effort += v

            arm.advisory_effort = len(set(ae_effort))

            # arm.to_string()

        # Select best arm
        mus = [x.mu for x in active_arms]
        # print("Medias: {}".format(mus))
        # print(mus.index(max(mus)))
        best_arm = active_arms[mus.index(max(mus))]
        best_arm.to_string()

        low_bound_best_arm = best_arm.mu - best_arm.boundary
        # print("low bound: {}".format(low_bound_best_arm))
        aa = []
        for a in active_arms:
            if a != best_arm and a.mu+a.boundary < low_bound_best_arm:
                print("{} will be drop".format(a.name))
                pass
                # active_arms.pop(a)
            else:
                aa.append(a)
        active_arms = aa
        print(".... there are still {} active arms".format(len(active_arms)))
        # for a in active_arms:
        #     a.to_string()
        #     print("ADV EFF:{}".format(a.advisory_effort))

        if len(active_arms) == 1:
            print("EUREKA! An unique winning arm has been found")
            save_active_arms_to_file(active_arms, K, alg_name, metric, reward_order)
            return active_arms[0]

    print("Sorry! No satisfactory BAI output found")
    print("Best arm found:")
    best_arm.to_string()
    for k, v in best_arm.advisory_effort_q.items():
        print(k, v)
    print("Advisory effort on best arm: {}".format(best_arm.advisory_effort))
    if save_to_file:
        save_active_arms_to_file(active_arms, K, alg_name, metric, reward_order)

    return best_arm


class Arm(object):
    """
    Bandit Arm Class
    """
    def __init__(self, id, path):
        self.id = id
        self.path = path
        self.name = path.split("input.")[1]
        self.mu = 0.0
        self.cumsum = 0.0
        self.plays = 0
        self.boundary = None
        self.advisory_effort_q = {}
        self.advisory_effort = None
        self.rewards = []

    def to_string(self):
        """
        Prints Arm as string
        """
        print("id: {} :: name: {} :: mu: {} :: cumsum: {} :: plays: {} :: boundary: {}".format(self.id,
                                                                                               self.name,
                                                                                               self.mu,
                                                                                               self.cumsum,
                                                                                               self.plays,
                                                                                               self.boundary))


if __name__ == "__main__":

    tic = time.time()
    main()
    print("Execution time: {}".format(time.time() - tic))
