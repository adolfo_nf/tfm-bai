def main():
    print("Hello BAI!")


def choose_one_or_several_arms(a):
    return a


def draw_reward(arm):
    return None


def update_state():
    return None


def bai_algorithm(k_arms):
    """Best Arm Identification algorithms
    input : K arms whose reward distributions (ν1, . . . , νK) are unknown
    output: A recommended arm r ∈ {1, . . . , K}
    Loop
        Choose one or several arms
        Draw a reward from each selected arm
        Update the state (affects the next pick)
        if STOPPING CONDITION true then
            Output a recommendation r ∈ {1, . . . , K}
    """
    stop_condition = True
    while not stop_condition:
        arms = choose_one_or_several_arms(k_arms)
        for arm in arms:
            draw_reward(arm)
        update_state()
        if stop_condition:
            print("output_recommendation")


def sdadads():
    """
    input: K
    arms
    whose
    reward
    distributions(ν1, ..., νK)
    are
    unknown
    input: δ(significance
    level
    required)
    input: N(maximum
    number
    of
    rounds)
    output: Recommended
    arms
    R ⊆ {1, ..., K}
    ActiveArms ← {1, ..., K}
    μ ← {0, ..., 0}
    Plays ← {0, ..., 0}
    Loop
    for a ∈ ActiveArms do
    Draw
    a
    reward
    Xa
    from νa
    Update
    μa and Playsa
    BestArm ← arg
    max
    k
    μk

    LowBoundBestArm ← μBestArm − R

    q
    log(2·K·N / δ)
    2
    P
    laysBestArm
    Remove
    from ActiveArms all
    arms
    k
    such
    that
    μk + R

    qlog(2·K·N / δ)
    2
    P
    laysk
    < LowBoundBestArm

    if | ActiveArms | = 1 then
    return (ActiveArms)
    """
    return None


if __name__ == "__main__":
    main()
