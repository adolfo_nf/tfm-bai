import glob
import json
import os
import pandas as pd
import time
import sys


def load_reference_data(data_dir=None, data_set=None):
    """Load reference data
    :param data_dir: data directory
    :param data_set: data set (TREC5, ..., TREC8)
    :return qrels: relevance judgements on files
    """
    # default data dir
    if data_dir is None:
        data_dir = './../datos'

    if data_set is None:
        data_set = 'TREC5'

    qrels = pd.read_csv(data_dir + '/' + data_set + '/qrels.official',
                        sep=' ',
                        names=['query_id', 'Q0', 'document_name', 'value'])
    return qrels


def create_precision_files(data_dir, data_set, top_n):

    # data_dir = './../datos'
    # data_set = 'TREC5'
    # top_n = 10

    inputs_list = glob.glob(data_dir + '/' + data_set + '/todas_pooled/input.*')

    qrels = load_reference_data(data_dir, data_set)

    series_list = list()
    system_names = list()

    for input_file in inputs_list:
        print(os.path.basename(input_file))
        # input_filename = os.path.basename(input_file)

        input_data = pd.read_csv(input_file, delim_whitespace=True, names=['query_id', 'Q0', 'document_name',
                                                                           'position', 'weight', 'system_name'])

        # Select top_n files for each query id
        top_files = input_data.groupby('query_id').head(top_n)

        relevance = []
        for index, row in top_files.iterrows():
            sel_qrel = qrels[(qrels['query_id'] == row['query_id']) & (qrels['document_name'] == row['document_name'])]

            if sel_qrel.empty:
                # print("# file {} not found in qrels for query {} at system "
                #       "{}".format(row['document_name'],
                #                   row['query_id'],
                #                   input_data['system_name'].iloc[0]))
                relevance.append(0)
            else:
                relevance.append(sel_qrel.iloc[0]['value'])

        top_files = top_files.assign(value=pd.Series(relevance).values)

        # Sum all elements grouped by query id
        series_list.append(top_files.groupby('query_id')['value'].sum())
        # Names for systems
        system_names.append(os.path.basename(input_file).split(".")[1])

    print("Concatenating series... Please wait.")
    df = pd.DataFrame([list(x) for x in series_list], columns=[str(x) for x in list(set(qrels['query_id']))],
                      index=system_names)
    print("Saving to file...")
    df.to_csv('{}/{}/count_{}.csv'.format(data_dir, data_set, top_n), index=True, sep=' ', encoding='utf8')


def create_effort_files(data_dir, data_set, top_n):

    # data_dir = './../datos'
    # data_set = 'TREC5'
    # top_n = 10

    inputs_list = glob.glob(data_dir + '/' + data_set + '/todas_pooled/input.*')
    effort_dict = dict()
    for input_file in inputs_list:
        print(os.path.basename(input_file))
        # input_filename = os.path.basename(input_file)

        input_data = pd.read_csv(input_file, delim_whitespace=True, names=['query_id', 'Q0', 'document_name',
                                                                           'position', 'weight', 'system_name'])

        # Select top_n files for each query id
        top_files = input_data.groupby('query_id').head(top_n)
        advisory_effort_dict = dict()
        for k, v in top_files.groupby('query_id')['document_name']:
            # print("{} :: {}".format(k, list(v)))
            advisory_effort_dict[k] = list(v)
        effort_dict[str(os.path.basename(input_file).split('.')[1])] = advisory_effort_dict

    with open('{}/{}/effort_{}.csv'.format(data_dir, data_set, top_n), 'w') as effort_file:
        # Save effort dictionary to json
        json.dump(effort_dict, effort_file)


if __name__ == "__main__":

    tic = time.time()
    data_dir = './../datos'
    trec_directories = ['TREC8']  # ['TREC5', 'TREC6', 'TREC7', 'TREC8']
    precision_lengths = [10, 100]
    precision = True
    effort = True

    if precision:
        print("Create precision files [csv]")
        for precision_length in precision_lengths:
            for trec_directory in trec_directories:
                create_precision_files(data_dir, trec_directory, precision_length)

    if effort:
        print("Create effort files [json]")
        for precision_length in precision_lengths:
            for trec_directory in trec_directories:
                create_effort_files(data_dir, trec_directory, precision_length)

    print("Execution time: {}".format(time.time() - tic))
