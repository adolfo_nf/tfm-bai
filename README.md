# TFM_BAI

This repository is about the Master's Degree Thesis on Best Arm Identification (BAI)

Best Arm Identification methods are a re evaluated on the quality of the 
recommended arm at the end of the exploration.

Some different algorithms are being implemented:

* Racing Algorithms (Hoeffding's Race, Bernstein's Race)
* Succesive Elimination Algorithm
* Median Elimination Algorithm
* LUCB 1 Algorithm

## Racing algorithms
Racing algorithms have been implemented as script

### Pre-requisites
* After clone, create __datos__ and __results__ folders next to __src__. They won't be added to repository.

* Store datasets (TREC5, TREC6, ...) in __datos__ folder. Each of these should have:
    * __todas_pooled__ folder containing input.* files for systems
    * __count_10.csv__ file
    * __count_100.csv__ file
    * __effort_10.csv__ file
    * __effort_100.csv__ file
    * __qrels.official__ file  
  
### Execute script
Go to __src__ directory and run:
```
python racing_algorithm_upgraded.py
```

#### Note:
This implementation of the algorithms is based on a pre-calculus of the number of relevant documents for 
each query and each system for metrics __p@10__ and __p@100__, to improve the performance so different test 
can be executed much faster.
  
### Modify execution
In __racing_algorithm_upgraded.py__ file modify __main()__ function. There, select:

* The data directory
* The data set: 'TREC5', 'TREC6', 'TREC7', 'TREC8'  
* The metric: 
    * 'p_at_10' precision at 10
    * 'p_at_100' precision at 100
* The way of selecting the queries: 
    * 'direct' queries from 251 to 300
    * 'random' random permutation of queries
    * 'reverse' queries from 300 to 251
* The algorithm: 'hoeffding' or 'berstein'
* The significance level __delta__: 0.05 (default), 0.1, ...
   
Also choose:
* __k_arms__ as  the number of arms for the race (None if you want to select all).
* __write_results__ A boolean value to write results (active arms after race's end) 
to a binary or json file. This file will be stored in __results__ 
directory with a name formed as:
```
final_arms_[number_of_arms_chosen]_[algorithm_name]_[metric]_[queries_order]_[delta]
```

If several runs are performed, file will be overwritten.

You can define the number of runs to perform, by modifying __n_runs_max__ value 


Racing algorithm will be executed from __main__ as:
```
bai = racing_algorithm('hoeffding', active_arms, 'p_at_10', 'direct', False)  
```

Resulting json files can be read from python console as:
```
import json

with open('./results/final_arms_[number_of_arms_chosen]_[algorithm_name]_[metric]_[queries_order]_[delta].json') as f:
    results = json.load(f)
for arm in results:
   print(arm)
```
Each arm (result) is a python dictionary.

## References
[1] [Jean-Yves Audibert, Sebastien Bubeck, and Rémi Munos. Best arm identification in multi-armed bandits. In  ́
Proceedings of the 23rd Annual Conference on Learning Theory (COLT), January 2010.](https://scholar.google.com/scholar?hl=en&q=Jean-Yves+Audibert%2C+S%C3%A9bastien+Bubeck%2C+and+R%C3%A9mi+Munos.+Best+arm+identification+in+multi-armed+bandits.+In+Proceedings+of+the+23rd+Conference+on+Learning+Theory+%28COLT%29%2C+2010.)

[2] [Jean-Yves Audibert, Remi Munos, and Csaba Szepesvári. Tuning bandit algorithms in stochastic environments.  ́
In Proceedings of the 18th International Conference on Algorithmic Learning Theory, ALT ’07, pages 150–165,
Berlin, Heidelberg, 2007. Springer-Verlag.](https://scholar.google.com/scholar?hl=en&as_sdt=0%2C5&q=Jean-Yves+Audibert%2C+Remi+Munos%2C+and+Csaba+Szepesv+%CC%81+ari.+Tuning+bandit+algorithms+in+stochastic+environments.+%CC%81+In+Proceedings+of+the+18th+International+Conference+on+Algorithmic+Learning+Theory%2C+ALT+%E2%80%9907%2C+pages+150%E2%80%93165%2C+Berlin%2C+Heidelberg%2C+2007.+Springer-Verlag.&btnG=)

[3] [Eyal Even-Dar, Shie Mannor, and Yishay Mansour. PAC bounds for multi-armed bandit and markov decision
processes. In Jyrki Kivinen and Robert H. Sloan, editors, Computational Learning Theory, pages 255–270, Berlin,
Heidelberg, 2002. Springer Berlin Heidelberg.](https://scholar.google.com/scholar?hl=en&as_sdt=0%2C5&q=Eyal+Even-Dar%2C+Shie+Mannor%2C+and+Yishay+Mansour.+PAC+bounds+for+multi-armed+bandit+and+markov+decision+processes.+In+Jyrki+Kivinen+and+Robert+H.+Sloan%2C+editors%2C+Computational+Learning+Theory%2C+pages+255%E2%80%93270%2C+Berlin%2C+Heidelberg%2C+2002.+Springer+Berlin+Heidelberg.&btnG=)

[4] [Eyal Even-Dar, Shie Mannor, and Yishay Mansour. Action elimination and stopping conditions for the multi-
armed bandit and reinforcement learning problems. J. Mach. Learn. Res., 7:1079–1105, December 2006.](https://www.cs.tau.ac.il/~mansour/papers/06jmlr.pdf)

[5] [Wassily Hoeffding. Probability inequalities for sums of bounded random variables. Journal of the American
statistical association, 58(301):13–30, 1963.](https://scholar.google.com/scholar?hl=en&as_sdt=0%2C5&q=Wassily+Hoeffding.+Probability+inequalities+for+sums+of+bounded+random+variables.+Journal+of+the+American+statistical+association%2C+58%28301%29%3A13%E2%80%9330%2C+1963.&btnG=)

[6] [Shivaram Kalyanakrishnan, Ambuj Tewari, Peter Auer, and Peter Stone. Pac subset selection in stochastic multi-
armed bandits. In Proceedings of the 29th International Coference on International Conference on Machine Learning, ICML’12, pages 227–234, USA, 2012. Omnipress.](https://scholar.google.com/scholar?hl=en&as_sdt=0%2C5&q=Shivaram+Kalyanakrishnan%2C+Ambuj+Tewari%2C+Peter+Auer%2C+and+Peter+Stone.+Pac+subset+selection+in+stochastic+multi-+armed+bandits.+In+Proceedings+of+the+29th+International+Coference+on+International+Conference+on+Machine+Learning%2C+ICML%E2%80%9912%2C+pages+227%E2%80%93234%2C+USA%2C+2012.+Omnipress.&btnG=)

[7] [Oded Maron and Andrew W. Moore. Hoeffding races: Accelerating model selection search for classification and
function approximation. In Jack D. Cowan, Gerald Tesauro, and Joshua Alspector, editors, NIPS, pages 59–66.
Morgan Kaufmann, 1993.](https://scholar.google.com/scholar?hl=en&as_sdt=0%2C5&q=Oded+Maron+and+Andrew+W.+Moore.+Hoeffding+races%3A+Accelerating+model+selection+search+for+classification+and+function+approximation.+In+Jack+D.+Cowan%2C+Gerald+Tesauro%2C+and+Joshua+Alspector%2C+editors%2C+NIPS%2C+pages+59%E2%80%9366.+Morgan+Kaufmann%2C+1993.&btnG=)

[8] [Volodymyr Mnih, Csaba Szepesvari, and Jean-Yves Audibert. Empirical bernstein stopping. In  ́ Proceedings of
the 25th International Conference on Machine Learning, ICML ’08, pages 672–679, New York, NY, USA, 2008.
ACM.](https://scholar.google.com/scholar?hl=en&as_sdt=0%2C5&q=Volodymyr+Mnih%2C+Csaba+Szepesvari%2C+and+Jean-Yves+Audibert.+Empirical+bernstein+stopping.+In+%CC%81+Proceedings+of+the+25th+International+Conference+on+Machine+Learning%2C+ICML+%E2%80%9908%2C+pages+672%E2%80%93679%2C+New+York%2C+NY%2C+USA%2C+2008.+ACM.&btnG=)